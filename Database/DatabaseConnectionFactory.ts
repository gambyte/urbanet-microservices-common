import { IDatabaseConnectionProvider } from './IDatabaseConnectionProvider';
import { IDatabaseConnection } from './IDatabaseConnection';

export class DatabaseConnectionFactory
{
    private static provider : IDatabaseConnectionProvider;

    public static SetProvider(provider:IDatabaseConnectionProvider) : void
    {
        this.provider = provider;
    }

    public static async GetConnection() : Promise<IDatabaseConnection>
    {
        return await this.provider.getConnection();
    }
}