import { IQuery } from './IQuery';
import { IDatabaseConnection } from './IDatabaseConnection';
import * as WebRequest from 'web-request';

export class DatabaseMicroserviceConnection implements IDatabaseConnection
{
    private ip:string;
    private port:number;

    constructor(ip:string, port:number)
    {
        this.ip = ip;
        this.port = port;
    }

    public close(): void 
    {
        // Left blank intentionally. Shouldnt do anything
    }

    public async execute<TResult>(query:IQuery<TResult>): Promise<TResult> 
    {
        let queryString = query.getQueryString();
        let result = await WebRequest.get(this.ip + ":"+this.port+"/executeQuery?query=" + queryString);
        if(result.statusCode == 200)
        {
            return query.translateResult(result.message);
        }
        else
        {
            throw Error("There was an error while sending the query. StatusCode: " + result.statusCode + ". Message: " +result.message);
        }
    }
}