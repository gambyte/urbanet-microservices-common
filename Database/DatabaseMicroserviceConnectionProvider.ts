import { DatabaseMicroserviceConnection } from './DatabaseMicroserviceConnection';
import { IDatabaseConnectionProvider } from './IDatabaseConnectionProvider';
import {IDatabaseConnection} from "./IDatabaseConnection";

export class DatabaseMicroserviceConnectionProvider implements IDatabaseConnectionProvider
{
    private ip:string;
    private port:number;

    constructor(ip:string, port:number)
    {
        this.ip = ip;
        this.port = port;
    }

    async getConnection () : Promise<IDatabaseConnection>
    {
        return new DatabaseMicroserviceConnection(this.ip, this.port);
    }
}