import {IQuery} from "./IQuery";

export interface IDatabaseConnection
{
    execute<TResult>(query:IQuery<TResult>) : Promise<TResult>;
    close();
}