import { IDatabaseConnection } from './IDatabaseConnection';
export interface IDatabaseConnectionProvider
{
    getConnection() : Promise<IDatabaseConnection>;
}