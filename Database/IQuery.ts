export interface IQuery<TResult>
{
    getQueryString():string;
    translateResult(result:any):TResult;
}